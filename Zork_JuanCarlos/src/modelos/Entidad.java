package modelos;

import javax.swing.JTextArea;

/**
 * @class Entidad
 * @brief Clase abstracta que representa cualquier entidad que este en el mapa
 * @author Juan Carlos Beltran
 * @date 31/05/2018
 * @since 1.0
 */
public abstract class Entidad {
	private String nombre; //@param nombre representa el nombre de la entidad
	private String descripcion; //@param descripcion representa la descripcion de la entidad
	
	/**
	 * @function Entidad (Constructor)
	 * @param nombre String el nombre de la entidad
	 * @param descripcion String la descripcion de la entidad
	 */
	public Entidad(String nombre, String descripcion) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	/**
	 * @brief metodo que devuelve el nombre de la entidad
	 * @return String nombre de la entidad
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @brief metodo que cambia el nombre de la entidad 
	 * @param nombre String por el que se cambia el nombre anterior
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @brief metodo que devuelve la descripcion de la entidad
	 * @return String descripcion de la entidad
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @brief metodo que cambia la descripcion del a entidad
	 * @param descripcion String por el que se cambia el nombre anterior
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	/**
	 * @brief metodo de interaccion de la entidad con el jugador
	 * @param e Entidad con la que interactua
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @return boolean para saber si despues de la interaccion la entidad tiene que ser eliminada
	 */
	public abstract boolean interactuar(Entidad e, JTextArea textArea);
}
