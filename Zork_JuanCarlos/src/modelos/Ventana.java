package modelos;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import java.awt.Color;
import javax.swing.border.LineBorder;

import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JTextArea;

/**
 * @class Ventana
 * @brief Clase que representa la ventana de la intefaz grafica del juego
 * @author Juan Carlos Beltran
 * @date 03/06/2018
 * @since 1.1
 */
public class Ventana extends JFrame {

	private JPanel contentPane; //@param contentPane JPanel donde se insertan todos los botones, JText, etc
	private Jugador jugador; //@param jugador es el jugador del juego
	private Enemigo reyBrujo; //@param reyBrujo es el jefe final del juego
	private Zona zonaFinal; //@param zonaFinal es la zona final del juego
	private Mapa mapa; //@param mapa es el mapa del juego
	private int movimientos; //@param movimientos es el numero de movimientos que el jugador hace en el juego

	/**
	 * @function Ventana (Constructor)
	 */
	public Ventana() {
		setResizable(false);

		this.jugador = new Jugador("Bob");

		// Creacion de la zona que contiene el Boss Final
		this.reyBrujo = new Enemigo("Rey Brujo", "Encuentras al Rey Brujo preparado para pelear contra ti", 8);
		this.zonaFinal = new Zona("Entras en los aposentos del Rey Brujo");
		zonaFinal.setElemento(reyBrujo);

		this.mapa = new Mapa(jugador);

		this.setTitle("Zork");
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventana.class.getResource("/modelos/zork_icono.jpg")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		panel_1.setBounds(10, 340, 564, 110);
		contentPane.add(panel_1);
		panel_1.setLayout(null);

		JButton btnNorte = new JButton("Ir al Norte");
		btnNorte.setBackground(new Color(175, 238, 238));
		btnNorte.setBounds(77, 9, 100, 23);
		panel_1.add(btnNorte);

		JButton btnOeste = new JButton("Ir al Oeste");
		btnOeste.setBackground(new Color(175, 238, 238));
		btnOeste.setBounds(10, 43, 100, 23);
		panel_1.add(btnOeste);

		JButton btnEste = new JButton("Ir al Este");
		btnEste.setBackground(new Color(175, 238, 238));
		btnEste.setBounds(138, 43, 100, 23);
		panel_1.add(btnEste);

		JButton btnSur = new JButton("Ir al Sur");
		btnSur.setBackground(new Color(175, 238, 238));
		btnSur.setBounds(77, 77, 100, 23);
		panel_1.add(btnSur);

		JButton btnAccion = new JButton("Acci\u00F3n");
		btnAccion.setVisible(true);
		btnAccion.setBackground(new Color(255, 228, 181));
		btnAccion.setBounds(287, 43, 236, 23);
		panel_1.add(btnAccion);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(new Color(240, 248, 255));
		scrollPane.setBounds(10, 11, 564, 318);
		contentPane.add(scrollPane);

		JTextArea textArea = new JTextArea();
		textArea.setEditable(false);
		scrollPane.setViewportView(textArea);

		JButton btnCoger = new JButton("Coger");
		btnCoger.setVisible(false);
		btnCoger.setBackground(new Color(255, 228, 181));
		btnCoger.setBounds(287, 43, 89, 23);
		panel_1.add(btnCoger);

		JButton btnNoCoger = new JButton("No Coger");
		btnNoCoger.setVisible(false);
		btnNoCoger.setBackground(new Color(255, 228, 181));
		btnNoCoger.setBounds(434, 43, 89, 23);
		panel_1.add(btnNoCoger);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setVisible(false);
		btnSalir.setBackground(new Color(255, 228, 181));
		btnSalir.setBounds(434, 43, 89, 23);
		panel_1.add(btnSalir);
		
		JButton btnComprar = new JButton("Comprar");
		btnComprar.setVisible(false);
		btnComprar.setBackground(new Color(255, 228, 181));
		btnComprar.setBounds(10, 43, 228, 23);
		panel_1.add(btnComprar);
		
		/*Actions de los botones*/
		
		btnNorte.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				jugador.setZonaActualY(jugador.getZonaActualY() - 1, textArea);
				jugador.setDescripcionVista(false);
				setMovimientos(getMovimientos() + 1);
				ronda(textArea, panel_1);
			}
		});

		btnOeste.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jugador.setZonaActualX(jugador.getZonaActualX() - 1, textArea);
				jugador.setDescripcionVista(false);
				setMovimientos(getMovimientos() + 1);
				ronda(textArea, panel_1);
			}
		});

		btnEste.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jugador.setZonaActualX(jugador.getZonaActualX() + 1, textArea);
				jugador.setDescripcionVista(false);
				setMovimientos(getMovimientos() + 1);
				ronda(textArea, panel_1);
			}
		});

		btnSur.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				jugador.setZonaActualY(jugador.getZonaActualY() + 1, textArea);
				jugador.setDescripcionVista(false);
				setMovimientos(getMovimientos() + 1);
				ronda(textArea, panel_1);
			}
		});

		btnAccion.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (mapa.getCasillas()[jugador.getZonaActualX()][jugador.getZonaActualY()].getElemento().getClass().getSimpleName().equals("Tienda")) {
					if(((Tienda)mapa.getCasillas()[jugador.getZonaActualX()][jugador.getZonaActualY()].getElemento()).getArticulo().size() > 0) {
						btnNorte.setVisible(false);
						btnOeste.setVisible(false);
						btnEste.setVisible(false);
						btnSur.setVisible(false);
						btnAccion.setVisible(false);
						btnComprar.setText("Comprar " + ((Tienda)mapa.getCasillas()[jugador.getZonaActualX()][jugador.getZonaActualY()].getElemento()).getArticulo().get(0).getNombre());
						btnComprar.setVisible(true);
						btnSalir.setVisible(true);
					}else {
						textArea.setText(textArea.getText() + "No ves nada en la tienda que sea de utilidad\n");
					}
				}else {
					btnNorte.setVisible(false);
					btnOeste.setVisible(false);
					btnEste.setVisible(false);
					btnSur.setVisible(false);
					btnAccion.setVisible(false);
					btnCoger.setVisible(true);
					btnNoCoger.setVisible(true);
				}
			}
		});
		
		btnCoger.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				mapa.getCasillas()[jugador.getZonaActualX()][jugador.getZonaActualY()].getElemento().interactuar(jugador, textArea);
				mapa.getCasillas()[jugador.getZonaActualX()][jugador.getZonaActualY()].setElemento(null);
				btnNorte.setVisible(true);
				btnOeste.setVisible(true);
				btnEste.setVisible(true);
				btnSur.setVisible(true);
				btnCoger.setVisible(false);
				btnNoCoger.setVisible(false);
			}
		});
		
		btnNoCoger.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnNorte.setVisible(true);
				btnOeste.setVisible(true);
				btnEste.setVisible(true);
				btnSur.setVisible(true);
				btnCoger.setVisible(false);
				btnNoCoger.setVisible(false);
				btnAccion.setVisible(true);

			}
		});
		
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				btnNorte.setVisible(true);
				btnOeste.setVisible(true);
				btnEste.setVisible(true);
				btnSur.setVisible(true);
				btnAccion.setVisible(true);
				btnComprar.setVisible(false);
				btnSalir.setVisible(false);
			}
		});
		
		btnComprar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(mapa.getCasillas()[jugador.getZonaActualX()][jugador.getZonaActualY()].getElemento().interactuar(jugador, textArea)) {
					btnComprar.setVisible(false);
				}
			}
		});
		
		this.setLocationRelativeTo(null);
		
		Connection con;
		try {
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/zork?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","1234");
			this.setVisible(true);
			ronda(textArea, panel_1);
		} catch (DatabaseException e1) {
			System.out.println((e1.mostrarMensaje()));
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
	}

	public Jugador getJugador() {
		return jugador;
	}

	public void setJugador(Jugador jugador) {
		this.jugador = jugador;
	}

	public Enemigo getReyBrujo() {
		return reyBrujo;
	}

	public void setReyBrujo(Enemigo reyBrujo) {
		this.reyBrujo = reyBrujo;
	}

	public Zona getZonaFinal() {
		return zonaFinal;
	}

	public void setZonaFinal(Zona zonaFinal) {
		this.zonaFinal = zonaFinal;
	}

	public Mapa getMapa() {
		return mapa;
	}

	public void setMapa(Mapa mapa) {
		this.mapa = mapa;
	}

	public int getMovimientos() {
		return movimientos;
	}

	public void setMovimientos(int movimientos) {
		this.movimientos = movimientos;
	}

	/**
	 * @brief metodo que verifica si hay alguna entidad en la casilla donde se encuentra el jugador e interactua con esa entidad
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @param panel JPanel que contiene los botones de la interfaz que se modifican segun haya una entidad u otra
	 */
	public void ronda(JTextArea textArea, JPanel panel) {
		// Codigo del juego

		((JButton) panel.getComponent(4)).setVisible(false);
		boolean perder = false;// Booleano para saber si el jugador muere o no en una pelea
		boolean ganar = false;// Booleano para saber si el jugador a matado al jefe final
		int coorXJugador;// Enteros que guardan la casilla en la que se encuentra el jugador
		int coorYJugador;
		boolean entidadVista = false;// Booleano para saber si ya se ha mostrado la descripcion de la entidad al jugador
		boolean entidadNull = true;// Booleano para saber si la casilla en la que se encuentra el jugador contiene una entidad

		coorXJugador = jugador.getZonaActualX();
		coorYJugador = jugador.getZonaActualY();
		entidadNull = true;

		if (jugador.getZonaActualX() == 2 && jugador.getZonaActualY() == -1) {
			textArea.setText(textArea.getText() + zonaFinal.getElemento().getDescripcion() + "\n\n");
			if (zonaFinal.getElemento().interactuar(jugador, textArea)) {// Interacciona con el enemigo. Entra si el jugador mata al enemigo
				zonaFinal.setElemento(null);// Elimina al enemigo de la casilla
				entidadNull = false;
				ganar = true;
			} else {
				perder = true;
			}
		} else {
			if (!jugador.isDescripcionVista()) {
				textArea.setText(textArea.getText() + mapa.getCasillas()[coorXJugador][coorYJugador].getDescripcion() + "\n\n");
				jugador.setDescripcionVista(true);
			}

			if (mapa.getCasillas()[coorXJugador][coorYJugador].getElemento() != null) {// Verifica si la casilla donde se encuentra el jugador contiene un elemento. Entra si lo contiene
				if (mapa.getCasillas()[coorXJugador][coorYJugador].getElemento().getClass().getSimpleName().equals("Enemigo")) {// Verifica si la casilla donde se encuentra el jugador contiene un enemigo. Entra si lo contiene
					textArea.setText(textArea.getText() + mapa.getCasillas()[coorXJugador][coorYJugador].getElemento().getDescripcion() + "\n");
					if (mapa.getCasillas()[coorXJugador][coorYJugador].getElemento().interactuar(jugador, textArea)) {// Interacciona con el enemigo. Entra si el jugador mata al enemigo
						mapa.getCasillas()[coorXJugador][coorYJugador].setElemento(null);// Elimina al enemigo de la casilla
						entidadNull = false;
					} else {
						perder = true;
					}
				}
				if (coorXJugador == jugador.getZonaActualX() && coorYJugador == jugador.getZonaActualY() && !perder && !ganar && entidadNull) {// Se repite mientras el jugador no cambie de casilla ni muera o mate al jefe final
					if (!entidadVista) {					
						textArea.setText(textArea.getText() + mapa.getCasillas()[coorXJugador][coorYJugador].getElemento().getDescripcion() + "\n");
						entidadVista = true;
					}
					((JButton) panel.getComponent(4)).setVisible(true);
					((JButton) panel.getComponent(4)).setText("Acercarse a " + mapa.getCasillas()[coorXJugador][coorYJugador].getElemento().getNombre());
				}
			}
		}

		if (perder) {
			textArea.setText(textArea.getText() + "\n-----------------\n\nGAME OVER\n\n-----------------");
			panel.setVisible(false);

		} else if (ganar) {
			textArea.setText(textArea.getText() + "\n-----------------\n\nHAS GANADO\n\n-----------------");
			panel.setVisible(false);

			Connection con;
			try {
				con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/zork?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","root","1234");
				Statement smt=(Statement) con.createStatement();
				int insercion1 = smt.executeUpdate("CREATE TABLE IF NOT EXISTS Movimientos (NumeroMovimientos INT(4))");
				int insercion2 = smt.executeUpdate("insert into movimientos values ('" + getMovimientos() + "')");
				ResultSet rs = smt.executeQuery("select min(NumeroMovimientos) from zork.Movimientos");
				System.out.println("El n�mero de movimientos que has hecho para ganar han sido: " + getMovimientos());
				while (rs.next()) {
					if(rs.getInt(1) > getMovimientos()) {
						System.out.println("Has hecho un nuevo record");
					}
					System.out.println("El record es de: " + rs.getInt(1));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
