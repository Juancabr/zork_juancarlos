package modelos;

import java.util.Random;

import javax.swing.JTextArea;

/**
 * @class Jugador
 * @brief Clase que representa al jugador del juego
 * @see Personaje
 * @author Juan Carlos Beltran
 * @date 31/05/2018
 * @since 1.0
 */
public class Jugador extends Personaje{
	private int dinero; //@param dinero numero que representa el dinero del jugador
	private int zonaActualX; //@param zonaActualX numero entre 0 y la longitud X del mapa -1. Representa la casilla actual en el eje X en la que se encuentra el jugador dentro del mapa 
	private int zonaActualY; //@param zonaActualY numero entre 0 y la longitud Y del mapa -1. Representa la casilla actual en el eje Y en la que se encuentra el jugador dentro del mapa 
	private int zonaMaximaX; //@param zonaMaximaX numero longitud X del mapa -1. Representa la casilla maxima en el eje X a la que puede moverse el jugador
	private int zonaMaximaY; //@param zonaMaximaX numero longitud Y del mapa -1. Representa la casilla maxima en el eje Y a la que puede moverse el jugador
	private int poderArma; //@param poderArma numero representa el poder del arma que tiene equipada el jugador. Se inicializa a 0
	private boolean descripcionVista = false;//@param booleano para saber si ya se ha mostrado la descripcion de la casilla al jugador. Tambien se utiliza en los Sets de zonaActualX/Y
	
	/**
	 * @function Jugador (Constructor)
	 * @brief construye un jugador a partir del nombre. La descripcion sera una cadena vacia, la fuerza se inicializa a 2, el dinero a 0, poderArma a 0 y descripcionVista en false
	 * @param nombre String representa el nombre del jugador
	 */
	public Jugador(String nombre) {
		super(nombre, "", 2);
		this.dinero = 0;
		this.descripcionVista = false;
		this.poderArma = 0;
	}

	/**
	 * @brief metodo que devuelve el dinero del jugador
	 * @return int dinero del jugador
	 */
	public int getDinero() {
		return dinero;
	}

	
	/**
	 * @brief metodo que cambia el dinero del jugador
	 * @param dinero int por el que cambia el dinero del jugador
	 */
	public void setDinero(int dinero) {
		this.dinero = dinero;
	}

	/**
	 * @brief metodo que devuelve la casilla actual en el eje X en la que se encuentra el jugador dentro del mapa
	 * @return int numero de la zona actual X en la que se encuentra el jugador
	 */
	public int getZonaActualX() {
		return zonaActualX;
	}

	
	/**
	 * @brief metodo que cambia la posicion del jugador en el eje X del mapa
	 * @param zonaActualX int numero al que se quiere cambiar al jugador
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @pre Si la zona actual es menor que 0 o es mayor o igual que la zona maxima X no se cambiara 
	 */
	public void setZonaActualX(int zonaActualX, JTextArea textArea) {
		if(zonaActualX >= this.getZonaMaximaX() || zonaActualX < 0) {
			textArea.setText(textArea.getText() + "\nPor ah� no es posible ir\n\n");
		} else {
			if(this.getZonaActualX() > zonaActualX && this.isDescripcionVista()) {
				textArea.setText(textArea.getText() + "\nDecides ir hacia el Oeste\n\n");
			} else if (this.getZonaActualX() < zonaActualX && this.isDescripcionVista()){
				textArea.setText(textArea.getText() + "\nDecides ir hacia el Este\n\n");
			}
			this.zonaActualX = zonaActualX;
		}
	}
	
	/**
	 * @brief metodo que devuelve la casilla actual en el eje Y en la que se encuentra el jugador dentro del mapa
	 * @return int numero de la zona actual Y en la que se encuentra el jugador
	 */
	public int getZonaActualY() {
		return zonaActualY;
	}

	/**
	 * @brief metodo que cambia la posicion del jugador en el eje Y del mapa
	 * @param zonaActualY int numero al que se quiere cambiar al jugador
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @pre Si la zona actual es menor que 0 o es mayor o igual que la zona maxima Y no se cambiara. Pero si el jugador se encuentra en la casilla 2|0 y quiere restar 1 a la zona actual Y, s� que se cambiar�
	 */
	public void setZonaActualY(int zonaActualY, JTextArea textArea) {
		if(this.getZonaActualX() == 2 && zonaActualY < 0) {
			textArea.setText(textArea.getText() + "\nDecides ir hacia el Norte\n\n");
			this.zonaActualY = zonaActualY;
		}
		else if(zonaActualY >= this.getZonaMaximaY() || zonaActualY < 0) {
			textArea.setText(textArea.getText() + "\nPor ah� no es posible ir\n\n");
		} else {
			if(this.getZonaActualY() > zonaActualY && this.isDescripcionVista()) {
				textArea.setText(textArea.getText() + "\nDecides ir hacia el Norte\n\n");
			} else if (this.getZonaActualY() < zonaActualY && this.isDescripcionVista()) {
				textArea.setText(textArea.getText() + "\nDecides ir hacia el Sur\n\n");
			}
			this.zonaActualY = zonaActualY;
		}
	}
	
	/**
	 * @brief metodo que devuelve la casilla maxima en el eje X a la que puede moverse el jugador
	 * @return int numero de la casilla maxima en el eje X a la que puede moverse el jugador
	 */
	public int getZonaMaximaX() {
		return zonaMaximaX;
	}

	/**
	 * @brief metodo que cambia la casilla maxima en el eje X a la que puede moverse el jugador
	 * @param zonaMaximaX int por el que se cambia la zona maxima X
	 */
	public void setZonaMaximaX(int zonaMaximaX) {
		this.zonaMaximaX = zonaMaximaX;
	}

	
	/**
	 * @brief metodo que devuelve la casilla maxima en el eje Y a la que puede moverse el jugador
	 * @return int numero de la casilla maxima en el eje Y a la que puede moverse el jugador
	 */
	public int getZonaMaximaY() {
		return zonaMaximaY;
	}

	/**
	 * @brief metodo que cambia la casilla maxima en el eje Y a la que puede moverse el jugador
	 * @param zonaMaximaY int por el que se cambia la zona maxima X
	 */
	public void setZonaMaximaY(int zonaMaximaY) {
		this.zonaMaximaY = zonaMaximaY;
	}

	/**
	 * @brief metodo que devuelve el poder del arma que tiene equipada el jugador
	 * @return int numero que representa el poder del arma que tiene equipada el jugador
	 */
	public int getPoderArma() {
		return poderArma;
	}

	/**
	 * @brief metodo que cambia el poder del arma que tiene equipada el jugador
	 * @param poderArma int por el que se cambia el poder del arma que tiene equipada el jugador
	 */
	public void setPoderArma(int poderArma) {
		this.poderArma = poderArma;
	}

	/**
	 * @brief metodo que devuelve si el jugador ha visto la descripcion de la casilla donde se encuentra o no
	 * @return boolean que representa si el jugador ha visto la descripcion de la casilla donde se encuentra o no
	 */
	public boolean isDescripcionVista() {
		return descripcionVista;
	}

	/**
	 * @brief metodo que cambia si el jugador ha visto la descripcion de la casilla donde se encuentra o no
	 * @param descripcionVista boolean que representa si el jugador ha visto la descripcion de la casilla donde se encuentra o no
	 */
	public void setDescripcionVista(boolean descripcionVista) {
		this.descripcionVista = descripcionVista;
	}

	/*METODOS*/
	
	/**
	 * @deprecated No se usa por implementacion de interfaz
	 */
	public boolean interactuar(Entidad entidad, JTextArea textArea) {
		return false;
	}

	/**
	 * @brief funcion que devuelve un numero entre 0 y la fuerza del jugador mas el poder del arma que tiene equipada
	 * @return int un numero aleatorio entre 0 y la fuerza del jugador mas el poder del arma que tiene equipada
	 */
	@Override
	public int atacar() {
		Random r = new Random();
		int ataque = r.nextInt(this.getFuerza()+1);
		
		return ataque + this.getPoderArma();
	}
}

