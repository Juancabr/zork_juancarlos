package modelos;

import java.util.ArrayList;

import javax.swing.JTextArea;

/**
 * @class Tienda
 * @brief Clase que reprensenta una tienda en el mapa
 * @see Entidad
 * @author Juan Carlos Beltran Rodriguez
 * @date 02/06/2018
 * @since 1.0
 */
public class Tienda extends Entidad {
	private ArrayList<ObjetoDa�o> articulo; //@param articulo son los objetos que el jugador puede comprar en la tienda

	/**
	 * @function Tienda (Constructor)
	 * @bief construye una tienda apartir del nombre y la descripcion
	 * @param nombre String que sera el nombre de la tienda
	 * @param descripcion String que sera la descripcion de la tienda
	 */
	public Tienda(String nombre, String descripcion) {
		super(nombre, descripcion);
		this.articulo = new ArrayList<ObjetoDa�o>();
	}

	/**
	 * @brief metodo que devuelve los objetos que el jugador puede comprar en la tienda
	 * @return los objetos que el jugador puede comprar en la tienda
	 */
	public ArrayList<ObjetoDa�o> getArticulo() {
		return articulo;
	}

	/**
	 * @brief metodo que cambia los objetos que el jugador puede comprar en la tienda
	 * @param articulo ArrayList por el que se cambian los objetos que el jugador puede comprar en la tienda
	 */
	public void setArticulo(ArrayList<ObjetoDa�o> articulo) {
		this.articulo = articulo;
	}

	/* METODOS */

	/**
	 * @brief metodo para que el jugador compre un objeto de la tienda
	 * @param elemento ObjetoDa�o es el objeto de la tienda que el jugador quiere comprar
	 * @param j Jugador el jugador que compra los objetos
	 * @param textArea JTextArea area donde se mostraran los mensajes de la interaccion
	 * @return boolean que devuelve true si el jugador a comprado un objeto, sino no
	 */
	public boolean comprar(ObjetoDa�o elemento, Jugador j, JTextArea textArea) {
		if (j.getDinero() >= elemento.getValorCompra()) {// Verifica si el jugador tiene suficiente dinero
			j.setDinero(j.getDinero() - elemento.getValorCompra());// Le resta al jugador el dinero que tiene menos lo que cuesta el elemento
			j.setFuerza(j.getFuerza() * elemento.getPoder());// Le a�ade la fuerza del elemento al jugador
			
			textArea.setText(textArea.getText() + "Te tomas " + this.getArticulo().get(0).getNombre() + " y tu fuerza se multiplica\nAhora tienes " + (j.getFuerza() + j.getPoderArma()) +" de fuerza\n");
			
			this.articulo.remove(elemento);// Elimina el elemento del array de articulos
			return true;
		} else {
			textArea.setText(textArea.getText() + "No tienes suficiente dinero para comprar " + this.getArticulo().get(0).getNombre() + "\n");
			return false;
		}
	}
	
	/**
	 * @brief metodo de interaccion de la tienda con el jugador
	 * @param e Entidad con la que interactua
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @return boolean para saber si despues de la interaccion la entidad tiene que ser eliminada
	 */
	@Override
	public boolean interactuar(Entidad jugador, JTextArea textArea) {
		if(comprar(this.getArticulo().get(0), (Jugador)jugador, textArea)) {
			return true;
		}
		else {
			return false;
		}
	}
}