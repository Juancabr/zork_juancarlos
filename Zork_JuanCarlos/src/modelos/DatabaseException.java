package modelos;

import java.sql.SQLException;

/**
 * @class DatabaseException
 * @brief Clase sqlexception por si no se conecta a la base de datos
 * @author Juan Carlos Beltran
 * @date 31/08/2018
 * @since 1.0
 */
public class DatabaseException extends SQLException{
	private String mensaje; //@param mensaje cadena que se le mostrara al usuario
	
	/**
	 * @function DatabaseException (Constructor)
	 */
	public DatabaseException() {
		super();
		this.mensaje = "Error al conectar con la base de datos";
	}
	
	/**
	 * @brief metodo que devuelve el mensaje que se le mostrara al usuario
	 * @return String que se le mostrara al usuario
	 */
	public String mostrarMensaje() {
		return mensaje;
	}
}
