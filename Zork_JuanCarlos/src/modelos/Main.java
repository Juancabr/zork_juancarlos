package modelos;

/**
 * @class Main
 * @brief Clase que arranca el programa llamando al constructor de la clase Ventana
 * @author Juan Carlos Beltran
 * @date 31/05/2018
 * @since 1.0
 */
public class Main {

	/**
	 * @brief Se llama al constructor de Ventana
	 * @param args sin usar
	 */
	public static void main(String[] args) {	
		new Ventana();
	}
}