package modelos;

import java.util.Random;

import javax.swing.JTextArea;


/**
 * @class Enemigo
 * @brief Clase que representa a los enemigos del juego
 * @see Entidad
 * @author Juan Carlos Beltran
 * @date 31/05/2018
 * @since 1.0
 */
public class Enemigo extends Personaje{

	/**
	 * @function Enemigo (Constructor)
	 * @brief construye un enemigo a partir del nombre, la descripcion y la fuerza
	 * @param nombre String que sera el nombre del enemigo
	 * @param descripcion String que sera la apariencia del enemigo
	 * @param fuerza int que sera la fuerza que tendra el enemigo
	 */
	public Enemigo(String nombre, String descripcion, int fuerza) {
		super(nombre, descripcion, fuerza);
	}

	/*METODOS*/
	
	/**
	 * @brief funcion de lucha del jugador contra un enemigo
	 * @param jugador el jugador contra el que pelea el enemigo
	 * @param textArea area donde se mostraran los mensajes de la lucha al usuario
	 * @return boolean devuelve true si el jugador gana la pelea y mata al enemigo y false si pierde y muere
	 */
	public boolean luchaEnemigo(Jugador jugador, JTextArea textArea) {
		int ataqueEnemigo = this.atacar();
		int ataqueJugador = jugador.atacar();
		
		while(ataqueEnemigo == ataqueJugador) {
			ataqueEnemigo = this.atacar();
			ataqueJugador = jugador.atacar();
		}
		
		if(ataqueEnemigo<ataqueJugador) {
			textArea.setText(textArea.getText() + "Le haces " + ataqueJugador + " de da�o a " + getNombre() + " y este te hace " + ataqueEnemigo + " de da�o a ti.\nHas matado a " + getNombre() + "\n");
			return true;
		}
		else {
			textArea.setText(textArea.getText() + "Le haces " + ataqueJugador + " de da�o a " + getNombre() + " y este te hace " + ataqueEnemigo + " de da�o a ti.\nHas matado a  "+ getNombre() + "\n");
			return false;
		}
	}
	
	/**
	 * @brief funcion que llama a la lucha contra un enemigo
	 * @param jugador el jugador contra el que pelea el enemigo
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @return boolean devuelve true si el jugador gana la pelea y mata al enemigo y false si pierde y muere
	 */
	public boolean interactuar(Entidad jugador, JTextArea textArea) {
		if(luchaEnemigo((Jugador)jugador, textArea)) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	 * @brief funcion que devuelve un numero entre 0 y la fuerza del enemigo
	 * @return int un numero aleatorio entre 0 y la fuerza del enemigo
	 */
	@Override
	public int atacar() {
		Random r = new Random();
		
		return r.nextInt(this.getFuerza()+1);
	}
}
