package modelos;

import javax.swing.JTextArea;

/**
 * @class ObjetoDa�o
 * @brief Clase que representa un objeto del mapa que modifica el da�o del jugador
 * @author Juanca
 * @date 31/05/2018
 * @since 1.0
 * @see Entidad
 */
public class ObjetoDa�o extends Entidad{
	private int poder; // @param poder int la fuerza que le suma al jugador
	private int valorCompra; // @param valorCompra int cuanto cuesta el objeto en la tienda
	
	/**
	 * @function ObjetoDa�o (Constructor)
	 * @param nombre String el nombre del objeto
	 * @param descripcion String la descripcion del objeto
	 * @param poder int la fuerza que le suma al jugador
	 * @param valorCompra int cuanto cuesta el objeto en la tienda
	 */
	public ObjetoDa�o(String nombre, String descripcion, int poder, int valorCompra) {
		super(nombre, descripcion);
		this.poder = poder;
		this.valorCompra = valorCompra;
	}
	
	/**
	 * @brief metodo que devuelve el poder del objeto
	 * @return int el poder del objeto
	 */
	public int getPoder() {
		return poder;
	}
	
	/**
	 * @brief metodo que cambia el poder del objeto
	 * @param poder int por el que se cambia el poder anterior
	 */
	public void setPoder(int poder) {
		this.poder = poder;
	}
	
	/**
	 * @brief metodo que devuelve el valor de compra en la tienda del objeto
	 * @return int el valor de compra del objeto
	 */
	public int getValorCompra() {
		return valorCompra;
	}
	
	/**
	 * @brief metodo que cambia el valor de compra en la tienda del objeto
	 * @param valorCompra int por el que se cambia el valor de compra
	 */
	public void setValorCompra(int valorCompra) {
		this.valorCompra = valorCompra;
	}
	
	/*METODOS*/
	
	/**
	 * @brief metodo de interaccion del objeto con el jugador
	 * @param jugador Entidad con la que interactua
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @return boolean para saber si despues de la interaccion la entidad tiene que ser eliminada
	 */
	public boolean interactuar(Entidad jugador, JTextArea textArea) {
		((Jugador)jugador).setPoderArma(((Jugador)jugador).getPoderArma() + this.getPoder());//Se le suma a la fuerza del personaje el poder de la espada
		textArea.setText(textArea.getText() + "La espada te llena de determinaci�n\nAhora tu fuerza es de " + (((Jugador)jugador).getFuerza() + ((Jugador)jugador).getPoderArma()) + " puntos\n");
		return true;
	}
}
