package modelos;

import java.util.ArrayList;
import java.util.Random;

import javax.swing.JTextArea;

/**
 * @class Mapa
 * @brief Clase que representa el mapa donde estaran todas las zonas en las que estaran cada una de las entidades del juego
 * @author Juan Carlos Beltran
 * @date 03/06/2018
 * @since 1.1
 */
public class Mapa {
	private Zona[][] casillas; //@param casillas el conjunto de zonas que componen el mapa

	/**
	 * @function Mapa (Constructor)
	 * @param jugador Jugador que se utiliza aqui para situarle aleatoriamente en una zona en en mapa
	 */
	public Mapa(Jugador jugador) {
		Zona[][] casillas = new Zona[5][4];

		for (int i = 0; i < casillas.length; i = i + 1) {
			for (int j = 0; j < casillas[i].length; j = j + 1) {
				casillas[i][j] = new Zona("Esta es la descripcion de las casilla [" + i + "][" + j + "]");
			}
		}
		casillas[0][0] = new Zona("Te encuentras en un bosque oscuro");
		casillas[0][1] = new Zona("Te encuentras en la entrada de un bosque");
		casillas[0][2] = new Zona("Te encuentras en un campo de flores");
		casillas[0][3] = new Zona("Te encuentras en una playa");
		casillas[1][0] = new Zona("Te encuentras en un campo de rocas afiladas");
		casillas[1][1] = new Zona("Te encuentras en unas ruinas del bosque");
		casillas[1][2] = new Zona("Te encuentras en un claro del bosque");
		casillas[1][3] = new Zona("Te encuentras en un peque�o poblado");
		casillas[2][0] = new Zona("Te encuentras en un p�ramo muerto, miras al norte y ves la entrada a los aposentos del Rey Brujo");
		casillas[2][1] = new Zona("Te encuentras en un p�ramo desierto");
		casillas[2][2] = new Zona("Te encuentras en una cueva oscura");
		casillas[2][3] = new Zona("Te encuentras en un puerto mar�timo");
		casillas[3][0] = new Zona("Te encuentras en una ci�naga apestosa");
		casillas[3][1] = new Zona("Te encuentras en una monta�a nevada");
		casillas[3][2] = new Zona("Te encuentras en un sendero sinuoso");
		casillas[3][3] = new Zona("Te encuentras en algun sitio desconocido");
		casillas[4][0] = new Zona("Te encuentras en un bosque tropical");
		casillas[4][1] = new Zona("Te encuentras en un claro del bosque");
		casillas[4][2] = new Zona("Te encuentras en un camino");
		casillas[4][3] = new Zona("Te encuentras en la orilla de un lago");

		// Creo las entidades
		Enemigo orco1 = new Enemigo("Trasgo", "Te encuentras con un Trasgo sin espada, cuando te ve pega un grito y\nse dispone a atacar con las manos desnudas", 1);
		Enemigo orco2 = new Enemigo("Orco", "Te encuentras con un Orco medio borracho, est� dando espadazos al aire aleatoriamente", 1);
		Enemigo urukHai1 = new Enemigo("Uruk Hai", "Te encuentras con un Uruk Hai con ganas de cortar cabezas,\n cuando te ve, desenvaina su espada y se dirige hacia ti", 2);
		Enemigo urukHai2 = new Enemigo("Trol", "Te encuentras a un trol furioso", 2);
		Enemigo trol = new Enemigo("Balrog", "De repente ves que el cielo se oscurece y sientes un miedo terrible.\n Te das cuenta que de entre la oscuridad aparece un monstruo hecho fuego y sombra;\n un Balrog. Das un par de pasos atras lentamente pero te ve y cuando lo hace,\n hace aparecer una espada hecha de llamas de sus manos", 4);
		ObjetoDa�o espada = new ObjetoDa�o("Ira de Glorfindel", "Ves algo brillar a algunas metros de ti, se trata de una espada que perteneci�\na un gran guerrero elfo", 3, 10);
		ObjetoMonedas bolsa1 = new ObjetoMonedas("Bolsa de Monedas", "Te encuentras con una bolsa de monedas en el suelo", 10);
		ObjetoMonedas bolsa2 = new ObjetoMonedas("Bolsa de Monedas", "Te encuentras con una bolsa de monedas en el suelo", 10);
		ObjetoDa�o pocion = new ObjetoDa�o("Pocion de fuerza", "Descripcion de la pocion de fuerza", 2, 20);
		Tienda vendedor = new Tienda("Vendedor ambulante", "Escuchas algo acercarse detras tuya.\n Te das la vuelta y ves a un viejo con barba blanca con una mochila a sus espaldas");
		vendedor.getArticulo().add(pocion);

		// Inserto la entidades en el arrayList de elementos y creo el mapa
		ArrayList<Entidad> entidades = new ArrayList<>();
		entidades.add(orco1);
		entidades.add(orco2);
		entidades.add(urukHai1);
		entidades.add(urukHai2);
		entidades.add(trol);
		entidades.add(espada);
		entidades.add(bolsa1);
		entidades.add(bolsa2);
		entidades.add(vendedor);
		
		Random r = new Random();
		int x = r.nextInt(casillas.length);// Se crea un numero aleatorio para el punto X e Y
		int y = r.nextInt(casillas[0].length);

		while (entidades.size() > 0) {// El bucle acaba cuando la lista de entidades esta vacia
			while (casillas[x][y].getElemento() != null) {// Este bucle verifica si la zona que tenemos ya tiene una entidad y si es asi la cambia
				x = r.nextInt(casillas.length);
				y = r.nextInt(casillas[0].length);
			}
			casillas[x][y].setElemento(entidades.get(entidades.size() - 1));// Inserta la entidad en la zona correspondiente
			entidades.remove(entidades.size() - 1);// Elimina de la lista la entidad que se acaba de insertar en la zona
			x = r.nextInt(casillas.length);
			y = r.nextInt(casillas[0].length);

		}
		while (casillas[x][y].getElemento() != null) {// Este bucle verifica si la zona que tenemos ya tiene una entidad y si es asi la cambia
			x = r.nextInt(casillas.length);
			y = r.nextInt(casillas[0].length);
		}
		
		jugador.setZonaMaximaX(casillas.length);//Se introducen los limites del mapa
		jugador.setZonaMaximaY(casillas[0].length);
		jugador.setZonaActualX(x, new JTextArea());// Se introduce la casilla donde empieza el jugador
		jugador.setZonaActualY(y, new JTextArea());

		this.casillas = casillas;// Se inserta el array de zonas
	}

	/**
	 * @brief metodo que devuelve el array de zonas del que se compone el mapa
	 * @return Array[][] array de zonas del que se compone el mapa
	 */
	public Zona[][] getCasillas() {
		return casillas;
	}

	/**
	 * @brief metodo que cambia el array de zonas del que se compone el mapa
	 * @param casillas Array[][] por el que se cambia la zona
	 */
	public void setCasillas(Zona[][] casillas) {
		this.casillas = casillas;
	}
}
