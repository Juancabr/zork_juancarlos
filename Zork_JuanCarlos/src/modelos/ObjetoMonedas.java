package modelos;

import javax.swing.JTextArea;

/**
 * @class ObjetoMonedas
 * @brief Clase que representa un objeto del mapa que modifica el dinero del jugador
 * @author Juan Carlos Beltran
 * @date 31/05/2018
 * @since 1.0
 * @see Entidad
 */
public class ObjetoMonedas extends Entidad{
	private int moneda; //@param moneda int el numero que se sumara al dinero del jugador cuando este lo coja

	/**
	 * @function ObjetoMonedas (Constructor)
	 * @param nombre String el nombre del objeto
	 * @param descripcion String la descripcion del objeto
	 * @param moneda int numero de monedas que vale el objeto
	 */
	public ObjetoMonedas(String nombre, String descripcion, int moneda) {
		super(nombre, descripcion);
		this.moneda = moneda;
	}

	/**
	 * @brief metodo que devuelve el numero que se sumara al dinero del jugador cuando este lo coja
	 * @return int el numero que se sumara al dinero del jugador cuando este lo coja
	 */
	public int getMoneda() {
		return moneda;
	}

	/**
	 * @brief metodo que cambia el numero que se sumara al dinero del jugador cuando este lo coja
	 * @param moneda int por el que se cambia la moneda anterior
	 */
	public void setMoneda(int moneda) {
		this.moneda = moneda;
	}
	
	/*METODOS*/
	
	/**
	 * @brief metodo de interaccion del objeto con el jugador
	 * @param jugador Entidad con la que interactua
	 * @param textArea area donde se mostraran los mensajes de la interaccion
	 * @return boolean para saber si despues de la interaccion la entidad tiene que ser eliminada
	 */
	public boolean interactuar(Entidad jugador, JTextArea textArea) {
		((Jugador)jugador).setDinero((((Jugador)jugador).getDinero() + this.getMoneda()));//Se le suma al dinero del personaje la cantidad de dinero de la bolsa de monedas
		textArea.setText(textArea.getText() + "Ahora tienes " + ((Jugador)jugador).getDinero() + " monedas de oro\n");
		return true;
	}
}
