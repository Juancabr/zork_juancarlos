package modelos;

/**
 * @class Personaje
 * @brief Clase abstracta que representa un personaje que puede ser el jugador o un enemigo
 * @author Juan Carlos Beltran
 * @date 21/05/2018
 * @since 1.0
 * @see Entidad
 */
public abstract class Personaje extends Entidad{
	private int fuerza; //@param fuerza representa la fuerza del personaje

	/**
	 * @function Personaje (Constructor)
	 * @param nombre String el nombre del personaje
	 * @param descripcion String la descripcion del personaje
	 * @param fuerza int la fuerza del personaje
	 */
	public Personaje(String nombre, String descripcion, int fuerza) {
		super(nombre, descripcion);
		this.fuerza = fuerza;
	}

	/**
	 * @brief metodo que devuelve la fuerza del personaje
	 * @return int la fuerza del personje
	 */
	public int getFuerza() {
		return fuerza;
	}

	/**
	 * @brief metodo que cambia la fuerza del personaje
	 * @param fuerza int por el que se cambia la fuerza anterior
	 */
	public void setFuerza(int fuerza) {
		this.fuerza = fuerza;
	}
	
	/**
	 * @brief funcion de atacar del personaje
	 * @return int el ataque
	 */
	public abstract int atacar();
}