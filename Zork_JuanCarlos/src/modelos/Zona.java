package modelos;

/**
 * @class Zona
 * @brief Clase que representa una casilla del mapa donde puede haber una entidad
 * @author Juan Carlos Beltran
 * @date 02/06/2018
 * @since 1.0
 */
public class Zona {
	private String descripcion; //@param descripcion String que representa la descripcion de la zona
	private Entidad elemento; //@param Entidad representa la entidad que puede contener o no una zona  
	
	/**
	 * @function Zona (Constructor)
	 * @param descripcion String la descripcion de la zona
	 */
	public Zona(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Entidad getElemento() {
		return elemento;
	}


	public void setElemento(Entidad elemento) {
		this.elemento = elemento;
	}	
}
